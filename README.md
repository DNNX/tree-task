Compile:

```
    mvn clean compile assembly:single
```

Run:

```
    java -jar target/tree-task-1.0-SNAPSHOT-jar-with-dependencies.jar '[ 1 [ 2 3 ] 4 [ 5 [ 6 7 ] ] [ 8 ] ]'
```

Don't forget to put spaces between tokens.
