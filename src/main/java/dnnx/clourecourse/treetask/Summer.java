package dnnx.clourecourse.treetask;

import com.google.common.base.Function;

import java.math.BigInteger;
import java.util.List;

public class Summer implements Function<List<String>, BigInteger> {
    public BigInteger apply(List<String> strings) {
        BigInteger sum = BigInteger.ZERO;
        for (String string : strings) {
            sum = sum.add(new BigInteger(string));
        }
        return sum;
    }
}
