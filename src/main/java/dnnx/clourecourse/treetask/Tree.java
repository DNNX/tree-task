package dnnx.clourecourse.treetask;

import com.google.common.base.Function;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Tree<T> {
    private final Node<T> root;

    public Tree(Node<T> root) {
        if (root == null) {
            throw new IllegalArgumentException("root cannot be null");
        }

        this.root = root;
    }

    public Node<T> getRoot() {
        return root;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tree)) return false;

        Tree tree = (Tree) o;

        if (!root.equals(tree.root)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return root.hashCode();
    }

    public<U> Tree<U> map(Function<T, U> f) {
        return new Tree<U>(getRoot().map(f));
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(getRoot());
    }
}
