package dnnx.clourecourse.treetask;

import java.util.List;
import java.math.BigInteger;

public class App
{
    public static void main( String[] args )
    {
       if (args.length != 1) {
           System.out.println("Usage: java -jar tree-task.jar <tree>");
           System.out.println("Examples: java -jar tree-task.jar '[ 1 [ 2 3 ] 4 [ 5 [ 6 7 ] ] [ 8 ] ]'");
           System.out.println("Remember to put spaces between tokens.");
           System.exit(1);
       }

        Tree<List<String>> sourceTree = StringListTree.parse(args[0]);
        System.out.println("Source tree: " + sourceTree.toString());

        Tree<BigInteger> targetTree = sourceTree.map(new Summer());
        System.out.println("Summed tree: " + targetTree.toString());
    }
}
