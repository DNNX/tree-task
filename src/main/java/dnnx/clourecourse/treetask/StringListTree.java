package dnnx.clourecourse.treetask;

import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;

import java.util.ArrayList;
import java.util.List;

public class StringListTree extends Tree<List<String>> {

    public StringListTree(Node<List<String>> rootNode) {
        super(rootNode);
    }

    public static StringListTree parse(String input) {
        return parse(input.split(" "));
    }

    public static StringListTree parse(String[] input) {
        return new StringListTree(parseNode(Iterators.peekingIterator(Iterators.forArray(input))));
    }

    private static Node<List<String>> parseNode(PeekingIterator<String> tokens) {
        if (!tokens.hasNext())
            throw new IllegalStateException("Reached the end of stream");
        String token = tokens.next();
        if (!token.equals("[")) {
            throw new IllegalStateException("Unexpected token");
        }
        List<Node<List<String>>> children = new ArrayList<Node<List<String>>>();
        List<String> data = new ArrayList<String>();
        while (!tokens.peek().equals("]")) {
            token = tokens.peek();
            if (token.equals("[")) {
                Node<List<String>> child = parseNode(tokens);
                children.add(child);
            } else {
                data.add(token);
                tokens.next();
            }
        }
        tokens.next();
        return new Node<List<String>>(data, children);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
