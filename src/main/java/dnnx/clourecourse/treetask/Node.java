package dnnx.clourecourse.treetask;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

public class Node<T> {

    private final T data;
    private final List<Node<T>> children;

    public Node(T data, List<Node<T>> children) {
        this.data = data;
        this.children = children;
    }

    public Node(T data) {
        this.data = data;
        this.children = Collections.emptyList();
    }

    public T getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;

        Node node = (Node) o;

        if (data != null ? !data.equals(node.data) : node.data != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return data != null ? data.hashCode() : 0;
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public <U> Node<U> map(final Function<T, U> f) {
        return new Node<U>(f.apply(getData()), Lists.transform(children, lift(f)));
    }

    private<U> Function<Node<T>, Node<U>> lift(final Function<T, U> f) {
        return new Function<Node<T>, Node<U>>() {
            @Override
            public Node<U> apply(Node<T> node) {
                return node.map(f);
            }
        };
    }
}
