package dnnx.clourecourse.treetask;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import com.google.common.base.Function;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TreeTest {
    @Test
    public void mapsOneTreeToAnother() {
        Tree<String> sourceTree = new Tree<String>(
                new Node<String>("123",
                        asList(
                                new Node<String>("456"),
                                new Node<String>("1122",
                                        asList(
                                                new Node<String>("4455"),
                                                new Node<String>("6677")
                                        )
                                ),
                                new Node<String>("789")
                        )
                )
        );

        Tree<Integer> targetTree = new Tree<Integer>(
                new Node<Integer>(321,
                        asList(
                                new Node<Integer>(654),
                                new Node<Integer>(2211,
                                        asList(
                                                new Node<Integer>(5544),
                                                new Node<Integer>(7766)
                                        )
                                ),
                                new Node<Integer>(987)
                        )
                )
        );

        Function<String, Integer> reverseAndParse = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return Integer.parseInt(new StringBuffer(s).reverse().toString());
            }
        };

        assertEquals(targetTree, sourceTree.map(reverseAndParse));
    }
}