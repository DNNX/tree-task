package dnnx.clourecourse.treetask;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigInteger;
import java.util.Arrays;

@RunWith(JUnit4.class)
public class SummerTest {
    @Test
    public void sumTheNumbers() {
        assertEquals(new BigInteger("100"), new Summer().apply(asList("1", "-10", "109")));
    }

    @Test
    public void sumOfEmptyListIsZero() {
        assertEquals(BigInteger.ZERO, new Summer().apply(Arrays.<String>asList()));
    }
}
