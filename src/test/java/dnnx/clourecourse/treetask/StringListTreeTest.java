package dnnx.clourecourse.treetask;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith(JUnit4.class)
public class StringListTreeTest {

    @Test
    public void parsesSmallTree() {
        StringListTree tree = StringListTree.parse("[ 1 2 3 ]");
        StringListTree expected = new StringListTree(
                new Node<List<String>>(
                        asList("1", "2", "3")
                )
        );

        assertEquals(expected, tree);
    }

    @Test
    public void parsesExampleTree() {
        StringListTree tree = StringListTree.parse(new String[]{"[","1","[","2","3","]","4","[","5","[","6","7","]","]","[","8","]","]"});

        StringListTree expected = exampleTree();

        assertEquals(expected, tree);
    }

    private StringListTree exampleTree() {
        return new StringListTree(
                new Node<List<String>>(
                        asList("1", "4"),
                        asList(
                                new Node<List<String>>(
                                        asList("2", "3")
                                ),
                                new Node<List<String>>(
                                        asList("5"),
                                        asList(
                                                new Node<List<String>>(
                                                        asList("6", "7")
                                                )
                                        )
                                ),
                                new Node<List<String>>(
                                        asList("8")
                                )
                        )
                )
        );
    }
}